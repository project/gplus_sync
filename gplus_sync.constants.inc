<?php

  define('GPLUS_SYNC_MODULE_PATH', drupal_get_path('module', 'gplus_sync'));
  define('GPLUS_SYNC_URL_USER_PAGE', 'user/%user/gplus-sync');
  define('GPLUS_SYNC_URL_SETTINGS_PAGE', 'admin/config/people/gplus_sync');
  define('GPLUS_SYNC_URL_SIGN_ON', 'gplus_sync_sign_on');
  define('GPLUS_SYNC_URL_GPLUS_JS_CLIENT', 'https://plus.google.com/js/client:plusone.js');
  define('GPLUS_SYNC_BLOCK_ID_SIGN_IN_BUTTON', 'gplus_sync_sign_in_button');
  define('GPLUS_SYNC_HTML_BR', '<br/>');
  define('GPLUS_SYNC_HTML_NA', 'n/a');